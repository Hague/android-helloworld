
package com.matt.helloworld

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

public class MainActivity : AppCompatActivity() {

    override protected fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}
